package cache;

import java.io.IOException;
import java.util.LinkedHashMap;

public class Main {

    public static void main(String[] args) throws IOException {

        Cache cache = new Cache();
        //OutOfMemoryError, Cache Class
//        System.out.println(cache.getFile("C:\\^Stuff\\Test\\UlFiles\\bigfile.txt"));

        System.out.println("----------------------------------------------------------------------");

        System.out.println("**Normal test Cache Class**");
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file1.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file2.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file3.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file4.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file5.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file6.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file7.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file8.txt"));
        System.out.println(cache.getCacheList());

        System.out.println("----------------------------------------------------------------------");

        System.out.println("**Normal test Cache2 Class**");
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file1.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file2.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file3.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file4.txt"));
        System.out.println(cache.getCacheList());
        System.gc();
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file5.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file6.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file7.txt"));
        System.out.println(cache.getFile("src\\test\\resources\\test_files\\file8.txt"));
        System.out.println(cache.getCacheList());
    }
}
