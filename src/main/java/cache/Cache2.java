package cache;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.WeakHashMap;

public class Cache2 {

    private final WeakHashMap<String,String> cache = new WeakHashMap<>();

    public String getFile(String rootPath) throws IOException {
        File file = new File(rootPath);
        if(!file.isFile()){
            throw new RuntimeException("File path is invalid!");
        }

        addFile(rootPath, Files.readString(Path.of(rootPath)));
        return cache.get(rootPath);
    }


    public WeakHashMap<String, String> getCacheList(){
        return cache;
    }

    private void addFile(String pathName, String content){
            cache.put(pathName,content);
    }
}
