package cache;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;

public class Cache {

        private final LinkedHashMap<String,String> cache = new LinkedHashMap<>();
        public int capacity = 5;

    public void setCapacity(int capacity) {
        if(capacity > 0) {
            this.capacity = capacity;
        }
    }

    public String getFile(String rootPath) throws IOException {
        File file = new File(rootPath);
        if(!file.isFile()){
            throw new RuntimeException("File path is invalid!");
        }

        if(cache.get(rootPath) == null){
            addFile(rootPath, Files.readString(Path.of(rootPath)));
            return cache.get(rootPath);
        } else {
            var tempFile = cache.get(rootPath);
            cache.remove(rootPath);
            cache.put(rootPath,tempFile);
            return cache.get(rootPath);
        }
    }


    public LinkedHashMap<String, String> getCacheList(){
        return cache;
    }

    private void addFile(String pathName, String content){

            if(cache.size() == capacity){
                var leastUsedEntry = cache.keySet().iterator().next();
                cache.remove(leastUsedEntry);
            }
            cache.put(pathName,content);
    }
}
