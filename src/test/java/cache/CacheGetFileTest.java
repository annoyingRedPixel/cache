package cache;

import jdk.jfr.Description;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;

public class CacheGetFileTest {
    @Test
    @Description("Passing an invalid path so the app should throw an exception")
    void getFile() {
        Cache cache = new Cache();

        RuntimeException thrown = Assertions.assertThrows(RuntimeException.class, () -> {
            cache.getFile("Invalid_root_path");
        });

        Assertions.assertEquals("File path is invalid!", thrown.getMessage());
    }

    @Test
    @Description("Should add a file to the cache memory")
    void getFile2() throws IOException {
        Cache cache = new Cache();
        String actualFile = Files.readString(Path.of("src\\test\\resources\\test_files\\bigfile.txt"));

        String expectedFile = cache.getFile("src\\test\\resources\\test_files\\bigfile.txt");

        Assertions.assertEquals(actualFile, expectedFile);
    }

    @Test
    @Description("Adding files until we exceed the cache capacity, last files added should be removed")
    void getFile3() throws IOException {
        Cache cache = new Cache();
        cache.setCapacity(3);
        String file1 = cache.getFile("src\\test\\resources\\test_files\\file1.txt");
        String file2 = cache.getFile("src\\test\\resources\\test_files\\file2.txt");
        String file3 = cache.getFile("src\\test\\resources\\test_files\\file3.txt");
        String file4 = cache.getFile("src\\test\\resources\\test_files\\file4.txt");
        String file5 = cache.getFile("src\\test\\resources\\test_files\\file5.txt");

        String actualFile3 = Files.readString(Path.of("src\\test\\resources\\test_files\\file3.txt"));
        String actualFile4 = Files.readString(Path.of("src\\test\\resources\\test_files\\file4.txt"));
        String actualFile5 = Files.readString(Path.of("src\\test\\resources\\test_files\\file5.txt"));
        LinkedHashMap<String,String> actualMap = new LinkedHashMap<>();
        actualMap.put("src\\test\\resources\\test_files\\file3.txt",actualFile3);
        actualMap.put("src\\test\\resources\\test_files\\file4.txt",actualFile4);
        actualMap.put("src\\test\\resources\\test_files\\file5.txt",actualFile5);

        Assertions.assertEquals(actualMap,cache.getCacheList());
    }
}
